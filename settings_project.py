
PROJECT_NAME = 'imscam'

TRANSLATE_APPS = [
    'src/imscam'
]

TEST_APPS = [
    'imscam'
]

# for translations, different from settings.py
LANGUAGES = (
    ('en', 'English'),
    ('de', 'Deutsch'),
)

