from django.core import serializers
from django.core.exceptions import ImproperlyConfigured
from django.http import HttpResponse
from django.utils import simplejson
import os, pwd, grp

def drop_privileges(uid_name='nobody', gid_name='nogroup'):
    if os.getuid() != 0:
        # We're not root so, like, whatever dude
        return

    # Get the uid/gid from the name
    running_uid = pwd.getpwnam(uid_name).pw_uid
    running_gid = grp.getgrnam(gid_name).gr_gid

    # Remove group privileges
    os.setgroups([])

    # Try setting the new uid/gid
    os.setgid(running_gid)
    os.setuid(running_uid)

    # Ensure a very conservative umask
    old_umask = os.umask(077)

# helper function to instantiate class from string
def instantiate_class(spec):
    #if isinstance(spec, (tuple, list)):
    #    spec, args = spec[0], spec[1:]
    #else:
    #    args = []
    if isinstance(spec, basestring):
        module_path, class_name = spec.rsplit('.', 1)
        
        try:
            module = __import__(module_path, fromlist=[class_name])
        except ImportError, e:
            raise ImproperlyConfigured("Error importing class %s: '%s'" % (spec, e))
        try:
            instance = getattr(module, class_name)
            #print instance
            #instance = getattr(mod, attr)()
        except AttributeError, e:
            raise ImproperlyConfigured("Error importing class %s: '%s'" % (class_name, e))

        return instance
    else:
        raise ImproperlyConfigured('Invalid class spec: %s' % spec)
    

class JsonResponse(HttpResponse):
    def __init__(self, data):
        content = simplejson.dumps(data)
        super(JsonResponse, self).__init__(content=content, mimetype='application/json; charset=utf8')