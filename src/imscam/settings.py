from django.conf import settings
from imscam import utils
import sys
import os.path
APP_DIR = os.path.dirname(__file__)

from imscam.drivers.cam.base import BaseCamDriver

# get stuff that doesn't have to be set by user
# with getattr, providing default

#PTZ_DRIVER = getattr(settings, 'IMSCAM_PTZ_DRIVER', None)
PTZ_DRIVER = getattr(settings, 'IMSCAM_PTZ_DRIVER', 'imscam.drivers.ptz.ptzdummy')
#PTZ_DRIVER = getattr(settings, 'IMSCAM_PTZ_DRIVER', 'imscam.drivers.ptz.ptzdriver')

CAM_DRIVER = getattr(settings, 'IMSCAM_CAM_DRIVER', 'imscam.drivers.cam.dummy')
#CAM_DRIVER = getattr(settings, 'IMSCAM_CAM_DRIVER', 'imscam.drivers.cam.dslr')
#CAM_DRIVER = getattr(settings, 'IMSCAM_CAM_DRIVER', 'imscam.drivers.cam.raspicam')
#CAM_DRIVER = getattr(settings, 'IMSCAM_CAM_DRIVER', 'imscam.drivers.cam.video_cam')

if PTZ_DRIVER:
    PTZ_DRIVER = utils.instantiate_class(PTZ_DRIVER).PTZ()
#utils.drop_privileges('imscam', 'webapps')
try:
    #CAM_DRIVER = utils.instantiate_class(CAM_DRIVER).Cam()
    CAM_DRIVER = BaseCamDriver.cam_driver_factory(CAM_DRIVER)
except Exception as e:
    print e
    raise SystemExit()

# copy stuff from django settings so we
# don't need to import two settings files
MEDIA_ROOT = settings.MEDIA_ROOT
MEDIA_URL = settings.MEDIA_URL
PROJECT_ROOT = settings.PROJECT_ROOT

MEDIA_PREVIEW = getattr(settings, 'MEDIA_PREVIEW', os.path.join(MEDIA_URL, 'img', 'preview', 'preview.jpg'))  

PREVIEW_ROOT = getattr(settings, 'PREVIEW_ROOT', os.path.join(PROJECT_ROOT, 'media', 'img', 'preview'))
PREVIEW_FULL_PATH = getattr(settings, 'PREVIEW_FULL_PATH', '%s/preview.jpg' % PREVIEW_ROOT)
PREVIEW_TMP_ROOT = getattr(settings, 'PREVIEW_TMP_ROOT', '%s/tmp' % PREVIEW_ROOT)
GROUPS_IMG_ROOT = getattr(settings, 'GROUPS_IMG_ROOT', os.path.join(MEDIA_ROOT, 'img', 'groups'))
