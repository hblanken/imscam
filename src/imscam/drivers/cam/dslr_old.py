from imscam.drivers.cam.cam_driver import CamDriver

import threading
import traceback
import subprocess
import Image
import logging
from StringIO import StringIO
from piggyphoto import piggyphoto as pp
from piggyphoto.piggyphoto import libgphoto2error
#from Gphoto2Json.Gphoto2Json import gPhoto2setting

logger = logging.getLogger(__name__)

class Cam(CamDriver):
    
    def __init__(self):
        try:
            logger.debug("init dslr cam driver using piggyphoto")
            
            self._lock = threading.Lock()

            
            self._cam = None
            self._cam_keep_open = False 
            self._preview_running = False
            self._name = "dslr"
            
            self._width = None
            self._height = None
            
            self._shutterspeed_choices = None
            self._aperture_choices = None
            self._iso_choices = None
            self._whitebalance_choices = None
            self._settings = [] 
            
            
            self._init_cam_settings()
            
            self.__gphoto_shutterspeed_setting_path = '/main/capturesettings/shutterspeed'
            self.__gphoto_aperture_setting_path = '/main/capturesettings/aperture'
            self.__gphoto_iso_setting_path = '/main/imgsettings/iso'
            self.__gphoto_whitebalance_setting_path = '/main/imgsettings/whitebalance'
            
            #self.__capture_img_cmd = ['gphoto2', '--capture-image-and-download', '--stdout', '--quiet']
            
            
        except libgphoto2error as e:
            if e.result == -105:
                raise Exception("Unknown camera model")
            elif e.result == -60:
                raise Exception("Camera is in use")
            else:
                logger.error(e)
                logger.error(e.result)
                raise Exception("libgphoto error inside DSLR cam driver init")
        except Exception as e:
            logger.error(e)
            raise Exception("Exception inside DSLR cam driver init")
        
    
    def _init_cam_settings(self):
        self.init_cam()
        try:
            cur_shutterspeed = self._cam.config.main.capturesettings.shutterspeed.value
            self._shutterspeed_choices = self._get_choices(self._cam.config.main.capturesettings.shutterspeed)
            self._shutterspeed_setting = { 'name':'shutterspeed', 'current':cur_shutterspeed, 'choices':self._shutterspeed_choices }
            self._settings.append(self._shutterspeed_setting)
        except Exception as e:
            logger.debug(e)
            pass
        try:
            cur_aperture = self._cam.config.main.capturesettings.aperture.value
            self._aperture_choices = self._get_choices(self._cam.config.main.capturesettings.aperture)
            self._aperture_setting = { 'name':'aperture', 'current':cur_aperture, 'choices':self._aperture_choices }
            self._settings.append(self._aperture_setting)
        except Exception as e:
            logger.debug(e)
            pass
        try:
            cur_iso = self._cam.config.main.imgsettings.iso.value
            self._iso_choices = self._get_choices(self._cam.config.main.imgsettings.iso)
            self._iso_setting = { 'name':'iso', 'current':cur_iso, 'choices':self._iso_choices }
            self._settings.append(self._iso_setting)
        except Exception as e:
            logger.debug(e)
            pass
        try:
            cur_whitebalance = self._cam.config.main.imgsettings.whitebalance.value
            self._whitebalance_choices = self._get_choices(self._cam.config.main.imgsettings.whitebalance)
            self._whitebalance_setting = { 'name':'whitebalance', 'current':cur_whitebalance, 'choices':self._whitebalance_choices }
            self._settings.append(self._whitebalance_setting)
        except Exception as e:
            logger.debug(e)
            pass
        self.free_cam()
    
    def _get_choices(self, config):
        choices = []
        for i in range( config.count_choices() ):
            choice_val = config.get_choice(i)
            choice = (choice_val, choice_val)
            choices.append(choice)
        if not choices:
            return None
        return choices

    def _set_config(self, config, value):
        process = subprocess.Popen(['gphoto2', '--set-config', '%s=%s' % (config,value)], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = process.communicate()
    
    def init_cam(self):
        if not self._cam:
            try:
                self._cam = pp.Camera()
            except libgphoto2error as e:
                logger.error(e)
                raise e
            
    def free_cam(self):
        if self._cam and not self._cam_keep_open:
            del self._cam
            self._cam = None
    
    def get_name(self):
        return self._name
    
    def preview_running(self):
        return self._preview_running
    
    def set_width(self, value):
        self._width = value
        
    def set_height(self, value):
        self._height = value
    
    def get_shutterspeed_choices(self):
        return self._shutterspeed_choices
    
    def set_shutterspeed(self, value):
        self._set_config(self.__gphoto_shutterspeed_setting_path, value)
        self._shutterspeed_setting['current'] = value
    
    def get_aperture_choices(self):
        return self._aperture_choices
    
    def set_aperture(self, value):
        self._set_config(self.__gphoto_aperture_setting_path, value)
        self._aperture_setting['current'] = value
        
    def get_iso_choices(self):
        return self._iso_choices
    
    def set_iso(self, value):
        self._set_config(self.__gphoto_iso_setting_path, value)
        self._iso_setting['current'] = value
    
    def get_whitebalance_choices(self):
        return self._whitebalance_choices
    
    def set_whitebalance(self, value):
        logger.debug("setting whitebalance to %s" % value)
        self._set_config(self.__gphoto_whitebalance_setting_path, value)
        self._whitebalance_setting['current'] = value
    
    
    
    def get_settings(self):
        logger.debug("returning settings")
        return self._settings
    
    def set_setting(self, name, value):
        try:
            if self._preview_running:
                self.free_cam()
            func_name = "set_%s" % name
            if func_name in dir(self):
                getattr(self,func_name)(value)
                logger.debug("new %s setting: %s" % (name, value))
        except Exception as e:
            logger.error(e)
            pass
        finally:
            if self._preview_running:
                self.init_cam()
    
    
    
    def start_preview(self):
        try:
            self.init_cam()
            self._preview_running = True
        except libgphoto2error as e:
            if e.result == -105:
                raise Exception("Unknown camera model")
            elif e.result == -60:
                raise Exception("Camera is in use")
            else:
                raise Exception("Error starting preview")
        
    def stop_preview(self):
        self.free_cam()
        self._preview_running = False
        #pass
        
    def get_image(self, save_to=None):
        print "entering get_full_res_img()"
        image = None
        if not save_to:
            save_to = "tmp_image.jpg"
        try:
            self._lock.acquire()
            self.init_cam()
            self._cam.capture_image(save_to)
            image = Image.open(save_to)
        finally:
            if not self._preview_running:
                self.free_cam()
            self._lock.release()
            return image

    """def get_preview_img(self, size, save_to=None):
        if not self._preview_running:
            raise Exceptio("live preview must be running")
        if not save_to:
            save_to = "tmp_preview.jpg"
        self._lock.acquire()
        self._cam.capture_preview(save_to)
        self._lock.release()
        return Image.open(save_to).resize(size)
    
    def get_location_thumbnail(self, size, save_to=None):
        return self.get_preview_img(size, save_to)"""
        
    def get_preview_image(self, size, save_to=None):
        if not save_to:
            save_to = "tmp_preview_image.jpg"
        try:
            self._lock.acquire()
            self.init_cam()
            try: 
                self._cam.capture_preview(save_to)
                return Image.open(save_to).resize(size)
            except libgphoto2error as e:
                if e.result == -110:
                    if self._cam.config.main.capturesettings.autoexposuremode.value != 'A_DEP' and self._cam.config.main.capturesettings.autoexposuremode.value != 'Manual' and self._cam.config.main.capturesettings.autoexposuremode.value != 'AV' and self._cam.config.main.capturesettings.autoexposuremode.value != 'TV' and self._cam.config.main.capturesettings.autoexposuremode.value != 'P':
                        # there is no live view available for canon camera (450D)
                        self._cam.capture_image(save_to)
                        return Image.open(save_to).resize(size)
                else:
                    logger.error(e)
                    raise Exception("coult not capture preview image")
        finally:
            if not self._preview_running:
                self.free_cam()
            self._lock.release()
    
    def get_resized_image(self, size, save_to=None):
        if not save_to:
            save_to = "tmp_image.jpg"
        try:
            self._lock.acquire()
            self.init_cam()
            self._cam.capture_image(save_to)
            return Image.open(save_to).resize(size)
        except Exception as e:
            logger.error(e)
            raise Exception("could not capture resized image")
        finally:
            if not self._preview_running:
                self.free_cam()
            self._lock.release()
                

    
    def get_configured_image(self, cam_settings):
        # mby save current settings and reset after taking the image
        try:
            self._lock.acquire()
            for setting in cam_settings:
                self.set_setting(setting['name'], setting['value'])
            #self._lock.release()
            save_to = "tmp_scheduler_image"
            self.init_cam()    
            self._cam.capture_image(save_to)
            image = Image.open(save_to)    
            if self._width and self._height:
                image = image.resize((self._width, self._height))
            return image
            
        except libgphoto2error as e:
            if e.result == -105:
                raise Exception("Unknown camera model")
            elif e.result == -60:
                raise Exception("Camera is in use")
            else:
                raise Exception("Error capturing image for scheduled location")
        finally:
            if not self._preview_running:
                self.free_cam()
            self._lock.release()
        
        

    
    