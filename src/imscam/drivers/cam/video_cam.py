from .base import BaseCamDriver, CamDriverError

import cv2
import datetime
from PIL import Image
import logging


logger = logging.getLogger(__name__)

class CamDriver(BaseCamDriver):
    
    def __init__(self):
        BaseCamDriver.__init__(self)
        logger.debug("init video cam driver")
        self._name = "webcam"        
        self._init_cam_settings()
        #self._cam = cv2.VideoCapture(0)
        
    
    #def __del__(self):
    #    if self._cam:
    #        self._cam.release()
    #        self._cam = None
    
    def _init_cam_settings(self):
        pass
        
    
    def init_cam(self):
        try:
            if not self._cam:
                self._cam = cv2.VideoCapture(0)
                self._cam.read()
                self._cam.read()
                self._cam.read()
                self._cam.read()
        except Exception as e:
            logger.error(e)
            raise CamDriverError("error initializing camera")
    
            
    def free_cam(self):
        try:
            if  self._cam and not self._cam_keep_open:
                self._cam.release()
                self._cam = None
                pass
        except Exception as e:
            logger.error(e)
            raise CamDriverError("error freeing camera")
    
    
    def get_settings(self):
        return self._settings
    
    def get_preview_settings(self):
        return {}
    
    def set_setting(self, name, value):
        return
    
    def start_preview(self):
        try:
            self._lock.acquire()
            self.init_cam()
            self._preview_running = True
        except CamDriverError:
            raise
        except Exception as e:
            logger.error(e)
            raise CamDriverError("error starting live preview")
        finally:
            self._lock.release()
        
    def stop_preview(self):
        self._lock.acquire()
        self._preview_running = False
        try:
            self.free_cam()
        finally:
            self._lock.release()

    def get_preview_image(self, size, save_to=None):
        try:
            self._lock.acquire()
            
            ret, im = self._cam.read()
            if not ret:
                return None
            image = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
            image = Image.fromarray(image).resize(size)
            if save_to:
                image.save(save_to)
            return image
        except Exception as e:
            logger.error(e)
            raise CamDriverError("error taking preview image")
        finally:
            self._lock.release()
    
    
    def get_image(self, save_to=None):
        try:
            self._lock.acquire()
            self.init_cam()
            ret, im = self._cam.read()
            if not ret:
                return None
            image = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
            image = Image.fromarray(image)
            if save_to:
                image.save(save_to)
            return image
        except CamDriverError:
            raise
        except Exception as e:
            logger.error(e)
            raise CamDriverError("error taking image")
        finally:
            if not self._preview_running:
                self.free_cam()
            self._lock.release()
    
    def get_resized_image(self, size, save_to=None):
        try:
            self._lock.acquire()
            self.init_cam()
            ret, im = self._cam.read()
            if not ret:
                return None
            image = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
            image = Image.fromarray(image).resize(size)
            if save_to:
                image.save(save_to)
            return image
        except Exception as e:
            logger.error(e)
            raise CamDriverError("error taking resized image")
        finally:
            if not self._preview_running:
                self.free_cam()
            self._lock.release()
    
    def get_configured_image(self, cam_settings):
        width = None
        height = None
        
        for name, val in cam_settings.items():
            if name == 'width':
                width = val
            elif name == 'height':
                height = val
            else:
                self.set_setting(name, val)
        image = self.get_image()        
        if image and width and height:
            image = image.resize( (widht, height) ) 
        return image
 