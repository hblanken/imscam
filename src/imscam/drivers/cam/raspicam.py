import os
import io
import subprocess
from StringIO import StringIO
import time
import traceback
import logging
import datetime
from PIL import Image
import uuid
#from picamera import PiCamera
from picam import picam

from .base import BaseCamDriver, CamDriverError


logger = logging.getLogger(__name__)

class CamDriver(BaseCamDriver):
    
    def __init__(self):
        BaseCamDriver.__init__(self)
        logger.debug("init raspicam driver")
        self._name = "raspicam"
        
        self._init_cam_settings()
        
    
    def _init_cam_settings(self):
        try:
            self.init_cam()
            self._init_setting('iso','choice')
            self._init_setting('shutterspeed','choice')
            self._init_setting('whitebalance','choice')
            self._init_setting('exposure', 'choice')
        except Exception as e:
            logger.error(e)
        finally:
            self.free_cam()
    
    def _init_setting(self, name, type):
        func_name = "get_%s_choices" % name
        if func_name in dir(self):
            current = self._get_current_setting_value(name)
            setting = { 'type':type, 'current':current }
            if type == 'choice':
                choices = getattr(self,func_name)()
                setting['choices'] = choices
            self._settings[name] = setting
    
    def _get_current_setting_value(self, name):
        logger.debug("get current setting value for %s" % name)
        #logger.debug(self._cam)
        if name == 'iso':
            if picam.config.ISO == 0:
                return 'Auto'
            return picam.config.ISO
        elif name == 'shutterspeed':
            if picam.config.shutterSpeed == 0:
                return 'Auto'
            return picam.config.shutterSpeed
        else:
            return None
    
    
    def init_cam(self):
        return
        if not self._cam:
            #self._cam = PiCamera()
            #self._cam.resolution = self._cam.MAX_IMAGE_RESOLUTION
            self._cam = picam
            
    def free_cam(self):
        return
        if  self._cam and not self._cam_keep_open:
            #for port in list(self._cam._encoders):
            #    self._cam._stop_capture(port)
            #self._cam.close()
            self._cam = None
            del self._cam
    
    def get_settings(self):
        self._init_cam_settings()
        return self._settings
    
    def get_preview_settings(self):
        self._init_cam_settings()
        return {k:v for k,v in self._settings.items() if k != 'width' and k != 'height'}
    
    
    def set_setting(self, name, value):
        if not value:
            return
        if not self._preview_running:
            self.init_cam()
        try:
            func_name = "set_%s" % name
            if func_name in dir(self):
                getattr(self,func_name)(value)
        finally:
            if not self._preview_running:
                self.free_cam()
    
    
    def set_iso(self, value):
        if value == 'Auto':
            value = 0
        picam.config.ISO = int(value) 
        #self._iso = int(value)
        #self._cam.ISO = int(value)
        
    def set_shutterspeed(self, value):
        if value == 'Auto':
            value = 0
        picam.config.shutterSpeed = int(value) 
    
    def set_whitebalance(self, value):
        try:
            const = "MMAL_PARAM_AWBMODE_%s" % value.upper()
            const = getattr(picam, const)
            setattr(picam, 'config.awbMode', const)
        except Exception as e:
            logger.error(e)
            return
        
    def set_exposure(self, value):
        try:
            const = "MMAL_PARAM_EXPOSUREMODE_%s" % value.upper()
            const = getattr(picam, const)
            setattr(picam, 'config.exposure', const)
        except Exception as e:
            logger.error(e)
            return
    
    def set_width(self, value):
        #self._width = value
        pass
        
    def set_height(self, value):
        #self._height = value
        pass
    
    def get_shutterspeed_choices(self):
        try:
            self.init_cam()
            choices = []
            choices.append(('0', 'Auto'))
            choices.append(('1', '1/1000'))
            choices.append(('10', '1/100'))
            choices.append(('100', '1/10'))
            choices.append(('1000', '1'))
            choices.append(('2000', '2'))
            choices.append(('3000', '3'))
            choices.append(('5000', '5'))
            choices.append(('7000', '7'))
            choices.append(('10000', '10'))
            choices.append(('15000', '15'))
            choices.append(('20000', '20'))
            self.free_cam()
            return choices
        except Exception as e:
            print e
            self.free_cam()
            return None
        
    def get_aperture_choices(self):
        return None
        
    def get_iso_choices(self):
        try:
            self.init_cam()
            choices = []
            choices.append(('0', 'Auto'))
            choices.append(('100', '100'))
            choices.append(('200', '200'))
            choices.append(('300', '300'))
            choices.append(('400', '400'))
            choices.append(('800', '800'))
            self.free_cam()
            return choices
        except Exception as e:
            print e
        finally:
            self.free_cam()
            
        return None
        
    def get_whitebalance_choices(self):
        try:
            self.init_cam()
            choices = []
            choices.append(('off', 'Off'))
            choices.append(('auto', 'Auto'))
            choices.append(('sunlight', 'Sunlight'))
            choices.append(('cloudy', 'Cloudy'))
            choices.append(('shade', 'Shade'))
            choices.append(('tungsten', 'Tungsten'))
            choices.append(('fluorescent', 'Fluorescent'))
            choices.append(('incandescent', 'Incandescent'))
            choices.append(('flash', 'Flash'))
            choices.append(('horizon', 'Horizon'))
            self.free_cam()
            return choices
        finally:
            self.free_cam()
        return None
    
    def get_exposure_choices(self):
        try:
            self.init_cam()
            choices = []
            choices.append(('off', 'Off'))
            choices.append(('auto', 'Auto'))
            choices.append(('night', 'Night'))
            choices.append(('nightpreview', 'Nightpreview'))
            choices.append(('backlight', 'Backlight'))
            choices.append(('spotlight', 'Spotlight'))
            choices.append(('sports', 'Sports'))
            choices.append(('snow', 'Snow'))
            choices.append(('beach', 'Beach'))
            choices.append(('verylong', 'Verylong'))
            choices.append(('fixedfps', 'Fixedfps'))
            choices.append(('antishake', 'Antishake'))
            choices.append(('fireworks', 'Fireworks'))
            self.free_cam()
            return choices
        finally:
            self.free_cam()
        return None
    
    def start_preview(self):
        try:
            self.init_cam()
            #self._cam.start_preview()
            self._preview_running = True
        except:
            raise
        
    def stop_preview(self):
        try:
            #self._cam.stop_preview()
            self.free_cam()
            self._preview_running = False
        except:
            raise

    def get_image(self, save_to=None):
        image = picam.takePhoto()
        if save_to:
            image.save(save_to)
        return image
    
    def get_preview_image(self, size, save_to=None):
        image = picam.takePhotoWithDetails(size[0],size[1], 85)
        if save_to:
            image.save(save_to)
        return image
    
    def get_resized_image(self, size, save_to=None):
        image = picam.takePhotoWithDetails(size[0],size[1], 100)
        if save_to:
            image.save(save_to)
        return image    
        
    """def get_preview_img(self, size, save_to=None):
        if not self._preview_running:
            raise Exceptio("live preview must be running")
        if not save_to:
            save_to = "tmp_preview.jpg"
        try:
            stream = io.BytesIO()
            self._cam.capture(stream, format='jpeg')#, resize=(600, 400))
            stream.seek(0)
            return Image.open(stream).resize(size)
        except Exception as e:
            print e

        
    def get_location_thumbnail(self, size, save_to=None):
        return self.get_preview_img(size, save_to)"""
    
    
    
    
    def get_configured_image(self, cam_settings):
        
        width = None
        height = None
        
        for name, val in cam_settings.items():
            self.set_setting(name, val)
            if name == 'width':
                width = val
            elif name == 'height':
                height = val
        #self._lock.release()
        #save_to = "tmp_scheduler_image"
        self.init_cam()
        if width and height:
            image =  picam.takePhotoWithDetails(width, height, 100)
        else:
            image = picam.takePhoto()
        self.free_cam()
        return image
