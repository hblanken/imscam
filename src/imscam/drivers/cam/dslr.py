from .base import BaseCamDriver, CamDriverError

import threading
import traceback
import subprocess
from PIL import Image
import logging
from StringIO import StringIO

import libgphoto2wrapper.wrapper as wrapper
#from Gphoto2Json.Gphoto2Json import gPhoto2setting

logger = logging.getLogger(__name__)

class CamDriver(BaseCamDriver):
    
    def __init__(self):
        BaseCamDriver.__init__(self)
        self._name = "dslr"
        try:
            logger.debug("init dslr2 cam driver")
            self._config = None
            self._init_cam_settings()
        except CamDriverError:
            raise
        except Exception as e:
            logger.error(e)
            raise CamDriverError("Exception inside DSLR cam driver init")
        
    
    def _init_cam_settings(self):
        self.init_cam()
        self._init_setting('shutterspeed', 'choice')
        self._init_setting('aperture', 'choice')
        self._init_setting('iso', 'choice')
        self._init_setting('whitebalance', 'choice')
        self._init_setting('autoexposuremode', 'choice')
        self.free_cam()
    
    def _init_setting(self, name, type):
        try:
            widget = self._config.get_root_widget().get_child_by_name(name)
            #setting = { 'name':name, 'type':type, 'current':widget.get_value() }
            setting = { 'type':type, 'current':widget.get_value() }
            if type == 'choice':
                choices = self._get_choices(widget)
                setting['choices'] = choices
            setattr(self, "_%s_setting"%name, setting) 
            #self._settings.append(setting)
            self._settings[name] = setting
        except Exception as e:
                logger.debug(e)
                pass
    
    def _get_choices(self, widget):
        choices = []
        for choice in widget.get_choices():
            choices.append((choice, choice))
        if not choices:
            return None
        return choices

    def init_cam(self):
        if not self._cam:
            try:
                self._cam = wrapper.Camera()
                self._config = wrapper.Config(self._cam)
            except Exception as e:
                logger.error(e)
                raise CamDriverError("Could not initialize camera")
            
    def free_cam(self):
        
        if self._cam and not self._cam_keep_open:
            self._cam.release()
            self._cam = None
    
    
    def get_settings(self):
        self._init_cam_settings()
        logger.debug("returning settings")
        return self._settings
    
    def get_preview_settings(self):
        self._init_cam_settings()
        return {k:v for k,v in self._settings.items() if k != 'width' and k != 'height'}
    
    def set_setting(self, name, value):
        """
            set config before capture image. also for preview.
            can raise an exception
        """
        if not value:
            return
        if name == 'width' or name == 'height':
            # width and height can't be set on a DSLR cam
            return 
        
        try:
            self._lock.acquire()
            self.init_cam()
            
            logger.debug("change setting")
            
            widget = self._config.get_root_widget().get_child_by_name(str(name))
            widget.set_value(value)
            self._config.set_config()
            
            self._settings[name]['current'] = value
            #setting_name = "_%s_setting" % name
            #setting = getattr(self, setting_name)
            #setting['current'] = value
            
            logger.debug("name: %s, new value: %s" % (name, value))

        except Exception as e:
            logger.error(e)
            raise CamDriverError("Could not set %s to %s" % (name, value))
        finally:
            if not self._preview_running:
                self.free_cam()
            self._lock.release()
    
    
    def start_preview(self):
        try:
            self.init_cam()
            self._preview_running = True
        except Exception as e:
            logger.error(e)
            raise CamDriverError(e)
        
    def stop_preview(self):
        self.free_cam()
        self._preview_running = False
        #pass
        
    def get_image(self, save_to=None):
        #print "entering get_full_res_img()"
        if not save_to:
            save_to = "tmp_image.jpg"
        try:
            self._lock.acquire()
            self.init_cam()
            self._cam.capture_to_file(save_to)
            return Image.open(save_to)
        except Exception as e:
            logger.error(e)
            raise CamDriverError("could not capture image")
        finally:
            if not self._preview_running:
                self.free_cam()
            self._lock.release()

        
    def get_preview_image(self, size, save_to=None):
        if not save_to:
            save_to = "tmp_preview_image.jpg"
        try:
            self._lock.acquire()
            self.init_cam()
            self._cam.preview_to_file(save_to)
            return Image.open(save_to).resize(size)
        except Exception as e:
            logger.error(e)
            try:
                self._cam.capture_to_file(save_to)
                return Image.open(save_to).resize(size)
            except Exception as e:
                logger.error(e)
                raise CamDriverError("coult not capture preview image")
        finally:
            if not self._preview_running:
                self.free_cam()
            self._lock.release()
            
    
    def get_resized_image(self, size, save_to=None):
        if not save_to:
            save_to = "tmp_image"
        try:
            self._lock.acquire()
            self.init_cam()
            self._cam.capture_to_file(save_to)
            return Image.open("%s.jpg" % save_to).resize(size)
        except Exception as e:
            logger.error(e)
            raise CamDriverError('could not capture image')
        finally:
            if not self._preview_running:
                self.free_cam()
            self._lock.release()
                

    
    def get_configured_image(self, cam_settings):
        # mby save current settings and reset after taking the image
        try:
            width = None
            height = None
            self._cam_keep_open = True
            for name, value in cam_settings.items():
                if name == 'width':
                    width = value
                elif name == 'height':
                    height = value
                else:
                    self.set_setting(name, value)
        except:
            # ignore set_setting exception and continue 
            pass
        try:
            save_to = "tmp_scheduler_image"
            self._lock.acquire()
            self.init_cam()    
            self._cam.capture_to_file(save_to)
            image = Image.open("%s.jpg" % save_to)
            if width and height:
                image = image.resize( (width, height) )
            return image
        except Exception as e:
            logger.error(e)
            raise CamDriverError("Error capturing image for scheduled location")
        finally:
            self._cam_keep_open = False
            if not self._preview_running:
                self.free_cam()
            self._lock.release()
        
        

    
    