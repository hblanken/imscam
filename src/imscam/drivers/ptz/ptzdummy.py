#Benjamin Hoeller 0925688 TUWIEN 2013-2014
#this class makes the dummy connection between the server and virtual PTZInterface
 



class PTZ:

    def __init__(self):
        print "Initializing dummy PTZ dirver:"
        self.__photoCounter = 0
        self.__maxPhotoCounter = 10
        self.__U=180
        self.__V=90
        self.__d=10
        
    def goto(self, U, V):
        if U > 359:
            U = U - 360
        if U < 0:
            U = U + 360
            
        if V >= 180:
            V = 180
        if V <= 0:
            V = 0
            
        self.__U = U
        self.__V = V
        
    def getPan(self):
        return self.__U
    
    def getTilt(self):
        return self.__V
    
    def getLastDuration(self):
        return 0

    def increasePhotoCounter(self):
        self.__photoCounter += 1
        if self.__photoCounter >= self.__maxPhotoCounter:
            self.__photoCounter = 0
            self.reset()
            
    def reset(self):
        self.__init__()