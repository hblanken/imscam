#Benjamin Hoeller 0925688 TUWIEN 2013-2014
#this class makes the connection between the server and the PTZInterface
#it provides all necessary methods to move the head around
from django.http import HttpResponse
from imscam.settings import PTZ_DRIVER

p = PTZ_DRIVER
#if p:
#    p.goto(180,90)
d = 10

#moves the PTZInterface left for 'd' degrees
def ptz_moveleft(request):
    print "PTZ move left"
    U = p.getPan() + d
    V = p.getTilt()
    p.goto(U,V)
    return ptz_returnPosition()
    
#moves the PTZInterface right for 'd' degrees
def ptz_moveright(request):
    print "PTZ move right"
    U = p.getPan() - d
    V = p.getTilt()
    p.goto(U,V)
    return ptz_returnPosition()

#moves the PTZInterface up for 'd' degrees
def ptz_moveup(request):
    print "PTZ move up"
    U = p.getPan()
    V = p.getTilt() + d
    p.goto(U,V)
    return ptz_returnPosition()

#moves the PTZInterface down for 'd' degrees
def ptz_movedown(request):
    print "PTZ move down"
    U = p.getPan()
    V = p.getTilt() - d
    p.goto(U,V)
    return ptz_returnPosition()

#moves the PTZInterface to the given angles
def ptz_moveto(request):
    if request.method == "POST":
        pan = float(request.POST['pan'].replace(",","."))
        tilt = float(request.POST['tilt'].replace(",","."))
    else:
        pan=250
        tilt=120
    print "PTZ moving to position pan:"+str(pan)+" tilt:"+str(tilt)
    U = pan
    V = tilt
    p.goto(U,V)
    return ptz_returnPosition()

#initializes the positions and goes to t90,p180
def ptz_movecenter(request):
    print "center"
    #p.__init__()
    p.goto(180,90)
    return ptz_returnPosition()

def ptz_getposition(request):
    return ptz_returnPosition()

#returns the pan and tilt position in degrees as HttpResponse string
def ptz_returnPosition():
    return HttpResponse('reached position pan='+str(p.getPan())+' tilt='+str(p.getTilt())+' in '+str(p.getLastDuration())+' seconds')

