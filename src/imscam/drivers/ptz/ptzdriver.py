# Benjamin Hoeller 0925688 TUWIEN 2013-2014
# this is the main controller that moves the PTZHead around
import os
import ConfigParser
import stepperMotor
import datetime
import math
from multiprocessing import Process, Pipe

import logging
logger = logging.getLogger(__name__)

import fcntl

class Lock:
    
    def __init__(self, filename="/tmp/lock_ptz.tmp"):
        self.filename = filename
        # This will create it if it does not exist already
        self.handle = open(filename, 'w')
    
    # Bitwise OR fcntl.LOCK_NB if you need a non-blocking lock 
    def acquire(self):
        fcntl.flock(self.handle, fcntl.LOCK_EX)
        
    def release(self):
        fcntl.flock(self.handle, fcntl.LOCK_UN)
        
    def __del__(self):
        self.handle.close()
        
def str2bool(v):
    return v.lower() in ("yes", "true", "t", "1")

class PTZ:

    motor1 = stepperMotor
    motor2 = stepperMotor

    lastDuration = 0

    pan = 0
    tilt = 0

    maxPhotoCounter = 1
    
    # from https://wiki.python.org/moin/ConfigParserExamples
    def ConfigSectionMap(self, section):
        dict1 = {}
        options = self.Config.options(section)
        for option in options:
            try:
                dict1[option] = self.Config.get(section, option)
                if dict1[option] == -1:
                    DebugPrint("skip: %s" % option)
            except:
                dict1[option] = None
        return dict1
    
    def getPinArray(self,section):
        pin1 = int(self.ConfigSectionMap(section)['pin1'])
        pin2 = int(self.ConfigSectionMap(section)['pin2'])
        pin3 = int(self.ConfigSectionMap(section)['pin3'])
        pin4 = int(self.ConfigSectionMap(section)['pin4'])
        return pin1, pin2, pin3, pin4
    
    def initStepper(self,section):
        name = self.ConfigSectionMap(section)['name']
        pinArray = self.getPinArray(section)
        waitTime = float(self.ConfigSectionMap(section)['waittime'])
        sensorPin = int(self.ConfigSectionMap(section)['sensor'])
        negateSensor = str2bool(self.ConfigSectionMap(section)['negatesensor'])
        reverseInit = str2bool(self.ConfigSectionMap(section)['reverseinit'])
        sensorposition = int(self.ConfigSectionMap(section)['sensorposition'])
        globalMax = int(self.ConfigSectionMap(section)['globalmax'])
        angle = int(self.ConfigSectionMap(section)['angle'])
        backSteps = int(self.ConfigSectionMap(section)['backsteps'])
        return stepperMotor.stepper(name, pinArray, waitTime, sensorPin, negateSensor, reverseInit, sensorposition, globalMax, angle, backSteps)
   
    def __init__(self):
        print "Initializing PTZ interface:"
        print "Loading configuration File:"
        
        try:
            self._lock = Lock()
            self._lock.acquire()
        
            # configPath=os.getcwd()+'/config.txt'
            configPath = os.path.dirname(os.path.abspath(__file__)) + os.sep + 'config.txt'
            print configPath
            self.Config = ConfigParser.ConfigParser()
            self.Config.read(configPath)
            
            self.photoCounter = 0
            self.maxPhotoCounter = int(self.ConfigSectionMap("ptz")["initcounter"])
    	
            self.motor1 = self.initStepper("stepper1")
            self.motor2 = self.initStepper("stepper2")
            self.goto(180, 90)
    
            self.tilt = self.motor1.step2Deg(self.motor1.actualPosition)            
            self.pan = self.motor2.step2Deg(self.motor2.actualPosition)
        finally:
            self._lock.release()


    # increases the photoCounter, if it reaches maxPhotoCounter the device will be reinicialised!
    def increasePhotoCounter(self):
        try:
            self._lock.acquire()
            self.photoCounter += 1
            if self.photoCounter >= self.maxPhotoCounter:
                self.reset()
        finally:
            self._lock.release()

    # returns last reset value of pan stepper
    def getLastPanReset(self):
        return self.motor2.getLastReset()

    # returns last reset value of tilt stepper
    def getLastTiltReset(self):
        return self.motor1.getLastReset()


    # returns the steps of tilt axis
    def getU(self):
        return self.motor1.actualPosition

    # returns the steps of pan axis
    def getV(self):
        return self.motor2.actualPosition

    # returns the angle of tilt axis in degrees
    def getTilt(self):
        # return self.motor1.step2Deg(self.getU())
        return self.tilt

    # returns the angle of pan axis in degrees
    def getPan(self):
        # return self.motor2.step2Deg(self.getV())
        return self.pan

    # returns the duration of the last move
    def getLastDuration(self):
        return self.lastDuration

    # sets the new goal position in degrees
    def goto(self, pan, tilt):
        try:
            self._lock.acquire()
            if pan < 0:
                pan = 0
            elif pan > 360:
                pan = 360
            if tilt < 0:
                tilt = 0
            elif tilt > 180:
                tilt = 180 
                
            t = datetime.datetime.now()
    
            # self.gotoUV(self.motor1.deg2Step(tilt),self.motor2.deg2Step(pan))
            U = self.motor1.deg2Step(tilt)
            V = self.motor2.deg2Step(pan)
    	
            # to avoid gearwheel clearance we try to move to a point always from the same side 
            nU = U
            nV = V
            d = 150
            doBackStepps = False
    
            if tilt > self.tilt :
                doBackStepps = True
                maxDiff = self.motor1.maxValue - self.motor1.actualPosition
                nU = min(maxDiff, d) + U
            if pan > self.pan :
                doBackStepps = True
                maxDiff = self.motor2.maxValue - self.motor2.actualPosition
                nV = min(maxDiff, d) + V
            if doBackStepps :
                print 'doing ' + str(d) + ' backsteps'
                self.gotoUV(nU, nV)
    	
            self.gotoUV(U, V)
            self.pan = pan
            self.tilt = tilt	
            self.lastDuration = datetime.datetime.now() - t
    
            print "reached Position U: " + str(self.motor1.actualPosition) + " V: " + str(self.motor2.actualPosition)
            print "GOAL pan: " + str(self.getPan()) + " tilt: " + str(self.getTilt())
            print "REAL pan: " + str(self.motor2.step2Deg(self.motor2.actualPosition)) + " tilt: " + str(self.motor1.step2Deg(self.motor1.actualPosition))
            print "Duration: " + str(self.lastDuration) + " Seconds"
        finally:
            self._lock.release()
	

    # sets the new goal position in steps
    def gotoUV(self, U, V):

        self.motor1.setPosition(U)
        self.motor2.setPosition(V)

        # the move method for one stepper
        def move(motor, conn):
            while motor.isMoving():
                motor.move()
            conn.send([motor.actualPosition])

        p_conn1, c_conn1 = Pipe()
        p_conn2, c_conn2 = Pipe()

        # for each stepper the move() method gets called in a single thread
        p1 = Process(target=move, args=(self.motor1, c_conn1,))
        p2 = Process(target=move, args=(self.motor2, c_conn2,))

        p1.start()
        p2.start()
        p1.join()
        p2.join()
        
        # after finishing the threads the position must be saved	
        self.motor1.actualPosition = p_conn1.recv()[0]	 
        self.motor2.actualPosition = p_conn2.recv()[0]    

        
    
        # to save power while standing
        self.motor1.off()
        self.motor2.off()
	
    def reset(self):
        logger.debug("resetting ptz driver")
        self.__init__()
