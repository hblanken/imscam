from django.conf.urls import patterns, url
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic.base import TemplateView

from imscam import views
from imscam.drivers.ptz import ptzview
#import ptz_control
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    #url(r'^getconfig$', views.getconfig, name='getconfig'),
    #url(r'^autodetect$', views.autodetect, name='autodetect'),
    url(r'^preview$', views.get_preview_img, name='preview'),
    url(r'^sse_preview$', views.SsePreviewEvents.as_view(), name='sse_preview'),
    url(r'^start_preview$', views.start_preview, name='start_preview'),
    url(r'^stop_preview$', views.stop_preview, name='stop_preview'),
    #url(r'^group/new$', views.new_group, name='new_group'),
    #url(r'^group/save$', views.save_group, name='save_group'),
    #url(r'^group/locations/$', views.ajax_load_locations, name='load_locations'),

    url(r'^ptz_moveleft$', ptzview.ptz_moveleft, name='ptz_moveleft'),
    url(r'^ptz_moveright$', ptzview.ptz_moveright, name='ptz_moveright'),
    url(r'^ptz_moveup$', ptzview.ptz_moveup, name='ptz_moveup'),
    url(r'^ptz_movedown$', ptzview.ptz_movedown, name='ptz_movedown'),
    url(r'^ptz_movecenter$', ptzview.ptz_movecenter, name='ptz_movecenter'),
    url(r'^ptz_moveto$', ptzview.ptz_moveto, name='ptz_moveto'),
    url(r'^ptz_getposition$', ptzview.ptz_getposition, name='ptz_getposition'),
    
    
    #url(r'^snapshot/$', views.snapshot, name='snapshot'),
    
    url(r'^groups/$', views.groups, name='groups'), ###
    url(r'^groups/new/$', views.group_edit, name='group_new'),
    url(r'^groups/(?P<group_id>\d+)/edit/$', views.group_edit, name='group_edit'),
    url(r'^groups/(?P<group_id>\d+)/delete/$', views.group_delete, name='group_delete'),
    
    url(r'^groups/(?P<group_id>\d+)/locations/$', views.locations, name='locations'), ###
    url(r'^groups/(?P<group_id>\d+)/locations/new/$', views.location_edit, name='location_new'),
    url(r'^groups/(?P<group_id>\d+)/locations/(?P<location_id>\d+)/edit/$', views.location_edit, name='location_edit'),
    #url(r'^groups/(?P<group_id>\d+)/locations/load$', views.locations_load, name='locations_load'),
    
    url(r'^locations/update_pan_tilt/$', views.location_update_pan_tilt, name='location_update_pan_tilt'),
    url(r'^groups/locations/(?P<location_id>\d+)/delete/$', views.location_delete, name='location_delete'),
    
    #url(r'^location/$', views.location, name='location'),
    url(r'^copypreview/$', views.copy_preview, name='copypreview'),
    url(r'^groups/(?P<group_id>\d+)/locations/tmp_location_delete/$', views.tmp_location_delete, name='tmp_location_delete'),
    
    url(r'^play_pause/$', views.play_pause, name='play_pause'),
    
    url(r'^browse/$', views.browse_images, name='browse'),
    url(r'^browse/get_files/(?P<location_id>\d+)/$', views.get_files, name='get_files'),
    url(r'^browse/get_files/(?P<location_id>\d+)/(?P<page>\d+)/$', views.get_files, name='get_files_pagination'),
    url(r'^browse/get_latest_locationimage/(?P<locationimage_id>\d+)/$', views.get_latest_locationimage, name='get_latest_locationimage'),
    
    #url(r'^detail/$', views.ajax_get_location_details, name='detail'),
    
    url(r'^settings/$', views.cam_settings, name='cam_settings'),
    
    url(r'^settings/set_config$', views.set_config, name='set_config'),
    
    
    url(r'^help/$', TemplateView.as_view(template_name='imscam/help/about.html'), name='help'),
    url(r'^help/$', TemplateView.as_view(template_name='imscam/help/about.html'), name='help_about'),
    url(r'^help/create_group/$', TemplateView.as_view(template_name='imscam/help/create_group.html'), name='help_create_group'),
    url(r'^help/edit_group/$', TemplateView.as_view(template_name='imscam/help/edit_group.html'), name='help_edit_group'),
    url(r'^help/create_location/$', TemplateView.as_view(template_name='imscam/help/create_location.html'), name='help_create_location'),
    url(r'^help/edit_location/$', TemplateView.as_view(template_name='imscam/help/edit_location.html'), name='help_edit_location'),
    url(r'^help/live_preview/$', TemplateView.as_view(template_name='imscam/help/live_preview.html'), name='help_live_preview'),
    url(r'^help/live_preview_location/$', TemplateView.as_view(template_name='imscam/help/live_preview_location.html'), name='help_live_preview_location'),
    url(r'^help/move_pan_tilt/$', TemplateView.as_view(template_name='imscam/help/move_pan_tilt.html'), name='help_move_pan_tilt'),
    url(r'^help/update_pan_tilt/$', TemplateView.as_view(template_name='imscam/help/update_pan_tilt.html'), name='help_update_pan_tilt'),
    url(r'^help/schedule/$', TemplateView.as_view(template_name='imscam/help/schedule.html'), name='help_schedule'),
    
    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', views.logout, name='logout'),
)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
