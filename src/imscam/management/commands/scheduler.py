import os
import pytz
import time
import logging
from PIL import Image
#from time import time as curr_time
from datetime import datetime
from imscam import models
from imscam.settings import GROUPS_IMG_ROOT, CAM_DRIVER, PTZ_DRIVER
from imscam.models import Location, CaptureTime, LocationImage

from django.core.files import File
from django.core.management.base import BaseCommand, NoArgsCommand, CommandError
from django.db.models.fields import AutoField

logger = logging.getLogger(__name__)

class Command(NoArgsCommand):
    help = "Starts the scheduler. Close with CTRL+C"

    def capture_image(self, location):
        logger.info('scheduler takes image for %s' % location.name)
        #logger.debug("preview running___________________ " + str(CAM_DRIVER.preview_running()))
        start = time.time()
        
        group = location.group
        
        now = datetime.now(pytz.utc)
        dir = os.path.join(GROUPS_IMG_ROOT, group.name, location.name)
        filename = "%s" % (now.strftime('%Y%m%d_%H%M%S'))
        full_path = os.path.join(dir, filename + ".jpg")
        
        try:
            cam_settings = {}
            image_settings = location.image_settings
            for field in image_settings._meta.fields:
                if type(field) == AutoField :
                    continue
                cam_settings[field.name] =  getattr(image_settings, field.name)
            
            #logger.debug(cam_settings)
            image = CAM_DRIVER.get_configured_image(cam_settings)
            
            if not image:
                return
            
            image.save(full_path)
            
            logger.debug("captured image")
        
        except IOError as e:
            logger.error(e)
            return
        except Exception as e:
            logger.error(e)
            return None
        
        image = File(open(full_path, 'r'))
        
        size = (300, 200)
        im = Image.open(full_path)
        im = im.resize(size)
        thumbnail_path = os.path.join(dir, filename + "_thumb" + ".jpg")
        im.save(thumbnail_path)
        # file has to be removed because it is created a second time with image.save
        os.remove(full_path)
        
        logger.debug("capture time %s" % str(time.time() - start))
        
        location_img = LocationImage()
        location_img.time = now
        location_img.location = location
        location_img.name = filename
        
        """
        Saves a new file with the file name and contents provided. 
        This will not replace the existing file, but will create a new file and update the object to point to it. 
        If save is True, the model's save() method will be called once the file is saved.
        """    
        location_img.image.save(filename, image, save=True)
        
        
    def callback(self, location):
        capture_image(location)
    
    
    def check_location(self, location, default_capturetime_enabled):
        
        logger.debug('---------------------------------------')
        logger.debug('checking location %s' % location.name)
        #print "checking location %s" % location.name
        
        if not CAM_DRIVER:
            logger.error("no cam driver")
            raise NotImplementedError("no cam driver specified")
        
        capturetime = location.capture_time
                    
        if not default_capturetime_enabled or not location.group.capture_time.enabled or not capturetime.enabled:
            #print "not enabled"
            return False
        
        now = datetime.now(pytz.utc)
        now_date = now.date()
        now_time = now.time()
        weekday = now.weekday()
        
        
        start_date = capturetime.start_date
        end_date = capturetime.end_date
        
        latest_location_images = models.LocationImage.objects.filter(location=location.id)
        
        
        if latest_location_images:
            latest_image_datetime = latest_location_images.filter(location=location.id).latest('time').time
            dif_timedelta = now - latest_image_datetime 
            #if (dif_timedelta.seconds/60) < capturetime.interval:
            
            logger.debug("latest_image_date %s" % str(latest_image_datetime))
            logger.debug("interval %s" % str(capturetime.interval))
            logger.debug("dif %s" % str(dif_timedelta))
            
            if dif_timedelta.seconds < capturetime.interval:
                #print "not in interval"
                return False
        
        #print "checking weekdays..."
        if weekday == 0 and not capturetime.monday:
            return False
        elif weekday == 1 and not capturetime.tuesday:
            return False
        elif weekday == 2 and not capturetime.wednesday:
            return False
        elif weekday == 3 and not capturetime.thursday:
            return False
        elif weekday == 4 and not capturetime.friday:
            return False
        elif weekday == 5 and not capturetime.saturday:
            return False
        elif weekday == 6 and not capturetime.sunday:
            return False
        
        #print "checking date..."
        if start_date and end_date:
            if now_date < start_date or now_date > end_date:
                #print "not in date range"
                return False
        elif start_date:
            if now_date < start_date:
                #print "not in date range"
                return False
        elif end_date:
            if now_date > end_date:
                #print "not in date range"
                return False
        
        
        if capturetime.start_time and now_time < capturetime.start_time:
            #print "not in time range"
            return False
        if capturetime.end_time and now_time > capturetime.end_time:
            #print "not in time range"
            return False
        
        return True
    
    
    
    def handle_noargs(self, **options):
        
        X_SEC = 3 # if iteration in while loop takes less than X_SEC than wait difference
        
        try:
            while 1:
                
                start_iter = time.time()
                
                location_list = Location.objects.not_deleted()
                default_capturetime_enabled = CaptureTime.objects.get(default=True).enabled
                
                for location in location_list:                    
                    if self.check_location(location, default_capturetime_enabled):
                        # ptz_driver.move_to(location, callback)
                        # start subprocess for moving pt head and wait for process 
                        # pass
                        if PTZ_DRIVER and location.pan and location.tilt:
                            logger.debug("move pt head to %s,%s" % (str(location.pan), str(location.tilt)))
                            startMove = time.time()
                            PTZ_DRIVER.goto(location.pan, location.tilt)
                            logger.debug("move time %s" % str(time.time() - startMove))
                        try:
                            self.capture_image(location)
                        except Exception as e:
                            print e
                        
                        if PTZ_DRIVER:
                            logger.debug("scheduler increases ptz photoCounter")
                            PTZ_DRIVER.increasePhotoCounter()
                        

                end_iter = time.time()
                duration_iter = end_iter - start_iter
                if duration_iter < X_SEC:
                    diff_time = X_SEC - duration_iter
                    logger.debug("sleeping %s seconds..." % (int(diff_time)))
                    time.sleep(int(diff_time))
                #print "iter time: %s" % (end_iter-start_iter)
        except KeyboardInterrupt:
            return
    