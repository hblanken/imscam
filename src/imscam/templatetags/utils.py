from PIL import Image
from django import template
from imscam.models import LocationImage

register = template.Library()

@register.assignment_tag
def get_latest_locationimage(location_id):
    # TODO: exception handling
    locationimage_list = LocationImage.objects.filter(location=location_id)
    
    if not locationimage_list:
        return None
    
    return locationimage_list.latest('time')
        