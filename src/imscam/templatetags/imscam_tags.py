from django import template
#from sorl.thumbnail import get_thumbnail
from imscam import models, settings
from imscam.forms import GroupForm

register = template.Library()

@register.inclusion_tag('imscam/include/locations/location.html')
def render_location(location_id=False):
    if not location_id:
        return {}
        
    location = models.Location.objects.get(pk=location_id)
    im = "%simg/groups/%s/%s/location.jpg" % (settings.MEDIA_URL, location.group.name, location.name)
    #print im
    
    return {'location': location, 'im': im}
