from django import template
from string import Template
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

register = template.Library()

## tags.py
@register.simple_tag
def active(request, pattern):
    import re
    
    if pattern == "/imscam/":
        pattern = "^%s$" % pattern
    else:
        pattern = "^%s" % pattern
    if re.search(pattern, request.path):
        #print pattern
        return 'active'
    return ''
