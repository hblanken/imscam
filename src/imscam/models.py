import os 
import json
import datetime
from datetime import datetime
import distutils.core
import shutil
import logging

from django import forms
from django.db import models
from django.core.files import File
from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db.models.signals import post_save, post_delete

#from sorl.thumbnail import ImageField as SorlImageField
#from sorl.thumbnail import delete as sorl_delete
#from sorl.thumbnail import get_thumbnail

from imscam.settings import CAM_DRIVER, MEDIA_URL, GROUPS_IMG_ROOT

logger = logging.getLogger(__name__)


class NotDeletedQuerySet(models.query.QuerySet):
    def not_deleted(self):
        return self.filter(deleted=False)

class NotDeletedManager(models.Manager):
    use_for_related_fields = True

    def get_query_set(self):
        return NotDeletedQuerySet(self.model)

    def not_deleted(self, *args, **kwargs):
        return self.get_query_set().not_deleted(*args, **kwargs)


def get_image_path(instance, filename):
    #im = get_thumbnail(my_file, '100x100', crop='center', quality=99)
    return "img/groups/%s/%s/%s.jpg" % (instance.location.group.name, instance.location.name, filename)

def get_image_path2(instance, filename):
    #im = get_thumbnail(my_file, '100x100', crop='center', quality=99)
    return "img/groups/%s/%s/location.jpg" % (instance.group.name, instance.name)

class CaptureTime(models.Model):
    CAPTURE_SETTING_CHOICES = (
        ('default', 'Default settings'),
        ('custom', 'Custom settings')
    )
    
    default = models.NullBooleanField(default=None)
    
    enabled = models.BooleanField(default=False)
    
    
    start_date = models.DateField(null=True, blank=True)
    end_date   = models.DateField(null=True, blank=True)
    
    monday    = models.BooleanField(default=False)
    tuesday   = models.BooleanField(default=False)
    wednesday = models.BooleanField(default=False)
    thursday  = models.BooleanField(default=False)
    friday    = models.BooleanField(default=False)
    saturday  = models.BooleanField(default=False)
    sunday    = models.BooleanField(default=False)
    
    start_time = models.TimeField(null=True, blank=True)
    end_time   = models.TimeField(null=True, blank=True)
    interval   = models.PositiveIntegerField (
                        default=5,
                        validators = [
                            MinValueValidator(1)
                        ]
                    )     
    
    class Meta:
        permissions = (
            ("change_settings", "Can change scheduling settings"),
        )


class Group(models.Model):
    default = models.NullBooleanField(default=None)
    name = models.CharField(max_length=30, unique=True)
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)
    #enabled = models.BooleanField(default=False)
    deleted = models.BooleanField(default=False)
    
    capture_time = models.ForeignKey(CaptureTime, blank=True, null=True)
    
    #objects = models.Manager() # The default manager.
    #not_del_objects = NotDeletedManager()
    objects = NotDeletedManager()
    
    def delete(self):
        logger.info("deleting group %s" % self.name)
        for location in Location.objects.filter(group=self):
            location.delete()
        self.deleted = True
        self.name = "deleted_%s" % self.name
        self.save()
        
    def save(self):
        #if group new:
            #create new dir img/groups/name
        #else:
            #if group.name new:
                #create new dir
                #move recursive old to new
                #remove old
            #else:
                #pass
        orig_group = None
        
        if self.name.startswith("deleted_") and self.pk and self.deleted == False:
            self.name = self.name[8:]
        
        if self.pk is None:
            """dir = os.path.join(GROUPS_IMG_ROOT, self.name)
            print "new group save: dir: %s" % dir
            if not os.path.exists("%s" % dir):
                os.makedirs("%s" % dir)"""
            pass
                
        else:
            orig = Group.objects.get(pk=self.pk)
            if orig.name != self.name:
                orig_group = orig
                
        super(Group, self).save()
        
        if orig_group:
            src = os.path.join(GROUPS_IMG_ROOT, orig_group.name)
            dst = os.path.join(GROUPS_IMG_ROOT, self.name)
            logger.debug("group save: move %s to %s" % (src, dst))
            
            if os.path.exists(src):
                os.rename(src, dst)
            
            
            
            # update imagefield's url
            # TODO: mby too slow...
            for location in self.location_set.all():
                
                # upadte location thumbnail
                name = os.path.basename(location.image.name)
                path = os.path.join(GROUPS_IMG_ROOT, self.name, location.name, name)
                img = File(open(path), "r")
                os.remove(path)
                location.image.save(name[:-4], img, save=True)
            
                # update locationimages         
                for locationimage in location.locationimage_set.all():
                    name = os.path.basename(locationimage.image.name)
                    path = os.path.join(GROUPS_IMG_ROOT, self.name, location.name, name)
                    img = File(open(path), "r")
                    os.remove(path)
                    locationimage.image.save(name[:-4], img, save=True)
                    #sorl_delete(img, delete_file=False)
                    
                    
            
        
                
    def __unicode__(self):
        return u'%s' % (self.name,)

fields = {
    #'__module__': __name__,       
    #'width': models.PositiveIntegerField(blank=True, null=True),
    #'height': models.PositiveIntegerField(blank=True, null=True)
}
cam_settings = CAM_DRIVER.get_settings()
for name, setting in cam_settings.items():
    
    if setting['type'] == 'choice':
        default = setting['current']
        fields[name] = models.CharField(max_length=255, choices=setting['choices'], blank=True, null=True, default=default)
    elif setting['type'] == 'int':
        fields[name] = models.PositiveIntegerField(blank=True, null=True)
#print fields    
#ImageSetting = type('ImageSetting', (models.Model,), fields)

    

class ImageSetting(models.Model):
    pass
    #width          = models.PositiveIntegerField(blank=True, null=True)
    #height         = models.PositiveIntegerField(blank=True, null=True)
    #iso            = models.CharField(max_length=255, choices=CAM_DRIVER.get_iso_choices(), default='0')
    #whitebalance   = models.CharField(max_length=255, choices=CAM_DRIVER.get_whitebalance_choices(), default='0')
    #aperture       = models.CharField(max_length=255, choices=CAM_DRIVER.get_aperture_choices(), default='0')
    #shutterspeed   = models.CharField(max_length=255, choices=CAM_DRIVER.get_shutterspeed_choices(), default='0')
    #exposure_mode  = models.CharField(max_length=2, choices=CAM_DRIVER.EXPOSURE_MODE_CHOICES, default='0') 
    #awb_mode       = models.CharField(max_length=2, choices=CAM_DRIVER.AWB_MODE_CHOICES, default='0')
    #global_setting = models.BooleanField()
#from south.db import db
for name, field in fields.items():
    ImageSetting.add_to_class(name, field)
    #setattr(ImageSetting, name, field)
    #db.add_column('imagesetting', name, field)

class Location(models.Model):
    
    name = models.CharField(max_length=30)
    #thumbnail = models.ImageField(upload_to=get_image_path)
    image = models.ImageField(upload_to=get_image_path2, blank=True)
    
    pan = models.FloatField(validators = [MinValueValidator(0.0), MaxValueValidator(360)], blank=True, null=True)
    tilt = models.FloatField(validators = [MinValueValidator(0.0), MaxValueValidator(180)], blank=True, null=True)
    
    driver_name = models.CharField(max_length=255)
    
    tmp_preview = None
    tmp_pan = None
    tmp_tilt = None
    random = None
    
    group = models.ForeignKey(Group, blank=True, null=True)
    image_settings = models.ForeignKey(ImageSetting, blank=True, null=True)
    capture_time = models.ForeignKey(CaptureTime, blank=True, null=True)
    deleted = models.BooleanField(default=False)    
    
    #objects = models.Manager() # The default manager.
    #not_del_objects = NotDeletedManager()
    objects = NotDeletedManager()
    
    #class Meta:
    #    unique_together = ("name", "group")
        
    def save(self):
        #if wp new:
            #create dir img/groups/group_name/wp_name
            #save image in dir location.jpg
        #else:
            #if wp.name new:
                #create dir img/groups/group_name/wp_newname
                #move wp_thumb from old dir to new dir
                #remove old dir
            #else pass
        #if not self.interval and not self.crontab:
        #    raise ValidationError({'interval': ['One of interval or crontab must be set.']})
        orig_location = None
        created = False
        
        
        if self.pk is None:
            created = True
            src = self.tmp_preview
            dst_dir = os.path.join(GROUPS_IMG_ROOT, self.group.name, self.name)
            #dst = os.path.join(dst_dir, "location.jpg")
            logger.debug("src: %s" % src)
            #print "dst: %s" % dst
            if not os.path.exists("%s" % dst_dir):
                os.makedirs("%s" % dst_dir)
            
            #shutil.move("%s" % src, "%s" % dst)
            
            
            img = File(open(src, 'r'))
            # file has to be removed because it is created a second time with image.save
            os.remove(src)
            """
            Saves a new file with the file name and contents provided. 
            This will not replace the existing file, but will create a new file and update the object to point to it. 
            If save is True, the model's save() method will be called once the file is saved.
            """    
            self.image.save("location", img, save=False)
            
            
        else:
            logger.debug("location editing")
            orig = Location.objects.get(pk=self.pk)
            
            if self.deleted == False and self.name.startswith("deleted_"):
                self.name = self.name[8:]
            
            if orig.name != self.name:
                orig_location = orig
    
        super(Location, self).save()
        
        if orig_location:
            # location name changed.
            
            # rename location dir
            src = os.path.join(GROUPS_IMG_ROOT, orig_location.group.name, orig_location.name)
            dst = os.path.join(GROUPS_IMG_ROOT, self.group.name, self.name)
            os.rename(src, dst)
            
            # update locationimages         
            for locationimage in self.locationimage_set.all():
                name = os.path.basename(locationimage.image.name)
                path = os.path.join(GROUPS_IMG_ROOT, self.group.name, self.name, name)
                img = File(open(path), "r")
                os.remove(path)
                locationimage.image.save(name[:-4], img, save=True)
                #sorl_delete(img, delete_file=False)
                
            # upadte location thumbnail
            name = os.path.basename(self.image.name)
            path = os.path.join(GROUPS_IMG_ROOT, self.group.name, self.name, name)
            img = File(open(path), "r")
            os.remove(path)
            self.image.save(name[:-4], img, save=True)

    def delete(self):
        logger.info("deleting location %s" % self.name)
        self.deleted = True
        self.name = "deleted_%s" % self.name
        self.save()
        
    
    def __unicode__(self):
        return u'%s' % (self.name, )

    def admin_image(self):
        if self.image:
            return u'<img src="%s" />' % (self.image.url)
        else:
            return "No Image" 
    admin_image.short_description = 'Thumbnail'
    admin_image.allow_tags = True
    
    
    def get_tmp_image(self):
        return "%s/img/preview/tmp/%s.jpg" % (MEDIA_URL, self.random)
    #def image_thumb(self):
    #    return '<img src="/media/%s" width="100" height="100" />' % (self.thumbnail)
    #image_thumb.allow_tags = True



class LocationImage(models.Model):
    image = models.ImageField(upload_to=get_image_path)
    time = models.DateTimeField()
    name = models.CharField(max_length=15)
    
    location = models.ForeignKey(Location, null=True, blank=True)
    
    
    
    def save(self):
        super(LocationImage, self).save()
    
    class Meta:
        permissions = (
            ("view_recordings", "Can view captured images from scheduler"),
            ("view_liveimage", "Can view live preview image"),
        )
    
    
    def get_thumbnail_url(self):
        if self.image:
            return os.path.join(MEDIA_URL, "img", "groups", self.location.group.name, self.location.name, self.name + "_thumb.jpg")
        return None
    
    def __unicode__(self):
        if not self.location:
            return ""
        return u'location[%s]' % (self.location.name, )
    

#class LivePreviewState(models.Model):
#    running = models.BooleanField(default=False)