import logging
import datetime

from django import forms
from django.forms import ModelForm

from imscam.settings import CAM_DRIVER, PTZ_DRIVER
from imscam.models import Group, Location, ImageSetting, CaptureTime

logger = logging.getLogger(__name__)

class GroupForm(ModelForm):
    class Meta:
        model = Group
        fields = ['name',]
        
    def __init__(self, *args, **kwargs):
        super(GroupForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({'class' : 'form-control input-sm'})
        self.fields['name'].label = "Name *"
        
    
class GroupFormReadOnly(GroupForm):
    def __init__(self, *args, **kwargs):
        super(GroupForm, self).__init__(*args, **kwargs)
        #self.fields['name'].widget.attrs.update({'class': 'form-control'})
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class' : 'form-control input-sm', 'disabled': 'disabled'})
        
class LocationForm(ModelForm):
    class Meta:
        model = Location
        if PTZ_DRIVER:
            fields = ['name', 'pan', 'tilt']

        else:
            fields = ['name',]
            
        
    def __init__(self, *args, **kwargs):
        super(LocationForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({'class' : 'form-control input-sm'})
        self.fields['name'].label = "Name *"
        if PTZ_DRIVER:
            self.fields['pan'].widget = forms.HiddenInput()
            self.fields['tilt'].widget = forms.HiddenInput()

    # unique_together inside models was not working
    def clean_name(self):
        name = self.cleaned_data['name']

        for location in self.instance.group.location_set.not_deleted():
            if self.instance.id and self.instance.id == location.id:
                continue
            if name == location.name:
                raise forms.ValidationError("Name must be unique inside group")
    
        return name
            

        
class LocationFormReadOnly(LocationForm):
    class Meta:
        model = Location
        if PTZ_DRIVER:
            fields = ['name', 'pan', 'tilt']
        else:
            fields = ['name',]
        
    def __init__(self, *args, **kwargs):
        super(LocationFormReadOnly, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({'disabled': 'disabled'})
        if PTZ_DRIVER:
            self.fields['pan'].widget = forms.HiddenInput()
            self.fields['tilt'].widget = forms.HiddenInput()        
        
class ImageSettingForm(ModelForm):
    class Meta:
        model = ImageSetting
        fields = []
        for setting_name in CAM_DRIVER.get_settings():
            fields.append(setting_name)
            
        
    def __init__(self, *args, **kwargs):
        super(ImageSettingForm, self).__init__(*args, **kwargs)
        try: 
            self.fields['width'].widget.attrs.update({'placeholder': 'empty for max width'})
            self.fields['height'].widget.attrs.update({'placeholder': 'empty for max height'})
        except Exception as e:
            logger.error(e)
            pass
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class' : 'form-control input-sm'})
        
        
class ImageSettingFormReadOnly(ImageSettingForm):
    class Meta:
        model = ImageSetting
        #fields = ['width', 'height',]
        
    def __init__(self, *args, **kwargs):
        super(ImageSettingFormReadOnly, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({'disabled': 'disabled'})

class DefaultCaptureTimeForm(ModelForm):
    class Meta:
        model = CaptureTime
        exclude = ['default', 'option', 'start_date', 'end_date']
        
    def __init__(self, *args, **kwargs):
        super(DefaultCaptureTimeForm, self).__init__(*args, **kwargs)
        self.fields['enabled'].label = "Schedule"
        self.fields['start_time'].widget = forms.TimeInput(format='%H:%M')
        self.fields['start_time'].widget.attrs.update({'class': 'form-control input-sm tooltip-elem', 'data-toggle': 'tooltip', 'title': 'leave blank to capture all the time', 'placeholder': '00:00'})
        self.fields['end_time'].widget = forms.TimeInput(format='%H:%M')
        self.fields['end_time'].widget.attrs.update({'class': 'form-control input-sm tooltip-elem', 'data-toggle': 'tooltip', 'title': 'leave blank to capture all the time', 'placeholder': '23:59'})
        self.fields['interval'].label = "Interval in sec *"
        self.fields['interval'].widget.attrs.update({'class': 'form-control input-sm'})

class DefaultCaptureTimeFormReadOnly(DefaultCaptureTimeForm):
    class Meta:
        model = CaptureTime
        
    def __init__(self, *args, **kwargs):
        super(DefaultCaptureTimeFormReadOnly, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({'disabled': 'disabled'})

class CaptureTimeForm(DefaultCaptureTimeForm):
    class Meta:
        model = CaptureTime
        exclude = ['default',]        
        
    def __init__(self, *args, **kwargs):
        super(CaptureTimeForm, self).__init__(*args, **kwargs)
        self.fields['start_date'].widget.attrs.update({'class': 'start-date form-control input-sm tooltip-elem', 'data-toggle': 'tooltip', 'title': 'leave blank to capture all the time', 'placeholder': '01.01.2014'})
        self.fields['end_date'].widget.attrs.update({'class': 'end-date form-control input-sm tooltip-elem', 'data-toggle': 'tooltip', 'title': 'leave blank to capture all the time', 'placeholder': '31.12.2014'})
        
    def clean(self):
        cleaned_data = super(CaptureTimeForm, self).clean()
        
        start_date = cleaned_data.get('start_date')
        end_date = cleaned_data.get('end_date')
        
        today = datetime.date.today()
        if end_date and end_date < today:
            msg = u"end date must not be in the past!"
            self._errors["end_date"] = self.error_class([msg])
        if end_date and start_date and end_date < start_date:
            msg = u"end date must be after start date!"
            self._errors["end_date"] = self.error_class([msg])
            del cleaned_data['end_date'] 
            
        return cleaned_data

class CaptureTimeFormReadOnly(DefaultCaptureTimeFormReadOnly):
    class Meta:
        model = CaptureTime
        exclude = ['default',]
        
    def __init__(self, *args, **kwargs):
        super(CaptureTimeFormReadOnly, self).__init__(*args, **kwargs)
        self.fields['start_date'].widget.attrs.update({'class': 'start-date form-control input-sm', 'placeholder': '01.01.2014'})
        self.fields['end_date'].widget.attrs.update({'class': 'end-date form-control input-sm', 'placeholder': '31.12.2014'})
        for field in self.fields:
            self.fields[field].widget.attrs.update({'disabled': 'disabled'})
