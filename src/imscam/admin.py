from django.contrib import admin
from imscam.models import Group, Location, LocationImage

#from sorl.thumbnail import default
#ADMIN_THUMBS_SIZE = '60x60'

#from sorl.thumbnail.admin import AdminImageMixin

class LocationInline(admin.TabularInline):
    model = Location
    readonly_fields = ('admin_image',)
    exclude = ['image', 'image_settings', 'capture_time']
    list_display = ('name',)
    
    extra = 0
    
    

class GroupAdmin(admin.ModelAdmin):
    fields = ["start_date", "end_date", "name", "deleted"]
    inlines = [LocationInline]

class LocationImageAdmin(admin.ModelAdmin):
    fields = ["time","location"] 

admin.site.register(Group, GroupAdmin)
admin.site.register(LocationImage, LocationImageAdmin)