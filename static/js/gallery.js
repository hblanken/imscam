$(function() {
	
	
	
	
	$(".list-group a").click(function(event) {
		event.preventDefault();
    	$(".list-group a").removeClass("active");
    	var loc = $(this);
    	
        $.ajax({
        	type: "POST",
            url: loc.attr("href"),
            success: function (data) {
                $("div#main-content").html(data);
                loc.addClass("active");
            }
        });
    });
	
	$(document).on("click", ".collapse-section", function(event) {
		
		
		$(this).closest("div").find("div").toggle();
	    var span = $(this).find("span");
	    if (span.hasClass("glyphicon-chevron-down")) {
	        span.removeClass("glyphicon-chevron-down");
	        span.addClass("glyphicon-chevron-right");
	    } else {
	        span.removeClass("glyphicon-chevron-right");
	        span.addClass("glyphicon-chevron-down");
	    }
	});
	
		    
    /*$("ul li ul li.location").click(function() {
    	$(".browse-group-list li.active").removeClass("active");
    	var clicked_li = $(this);
        var location_id = $(this).find("input[type='hidden']").val();
        var url = $(this).data("url");
        clicked_li.addClass("active");
        $.ajax({
            url: url,
            type: "POST",
            success: function (data) {
                $("div#main-content").html(data);
                
            }
        });
    });*/
    
    
    $(document).on("click", ".pager a", function(event) {
    	event.preventDefault();
    	if ( $(this).attr('disabled') ) {
    		console.log("disabled");
    		return false;
    	}
    	var url = $(this).attr("href");
    	console.log(url);
    	$.ajax({
            url: url,
            type: "POST",
            success: function (data) {
                $("div#main-content").html(data);
                
            }
        });
    	event.preventDefault();
    });
    
    $(document).on("click", ".gallery-panel button", function(event) {
    	var full_res_url = $(this).closest(".thumbnail").find("input[type='hidden']").val();
    	console.log(full_res_url);
    	window.open(full_res_url);
    });
    
    
    
});