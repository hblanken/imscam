$(function() {
	$(document).on('click', '.bootbox', function (event) {
	    if(event.target != this) return;
	    $('.bootbox').modal('hide');
	});
});

var $loading = $('#spinner').hide();
$(document)
  .ajaxStart(function () {
    $loading.show();
  })
  .ajaxStop(function () {
    $loading.hide();
  });




function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');


function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
function sameOrigin(url) {
    // test that a given url is a same-origin URL
    // url could be relative or scheme relative or absolute
    var host = document.location.host; // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    // Allow absolute or scheme relative URLs to same origin
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
        // or any other URL that isn't scheme relative or absolute i.e relative.
        !(/^(\/\/|http:|https:).*/.test(url));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
            // Send the token to same-origin, relative URLs only.
            // Send the token only if the method warrants CSRF protection
            // Using the CSRFToken value acquired earlier
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});







$('#login-form').submit(function(e) {                                                                                                        
	e.preventDefault(); 
	
	$.ajax({                                                                                                                           
	    type: "POST",
	    url: $(this).attr('action'),                                                                                                    
	    data: $('#login-form').serialize()
	}).done(function(response) {
		console.log(response.next);
        // redirected to the required url
		window.location.replace(response.next);
	}).fail(function(xhr, ajaxOptions, thrownError) {
		if(xhr.status == 404) {
	    	displayAlert("danger", "Server unreachable");
	    } else {
	    	displayAlert('warning', 'login failed - username or password wrong');
	    }
		
	});
});

function displayAlert(alertClass, msg) {
	var $alert = $(".main-alert.alert-" + alertClass);
	$(".main-alert").hide();
	$alert.finish();
	//$alert.hide();
	$alert.html("<span>" + msg + "</span>");
	//$alert.fadeIn(50);
	$alert.show(50).delay(4000).fadeOut(50);
}


$(function() {
	$(".tooltip-elem").tooltip();
});


var ajaxFailCallback = function(xhr, textStatus, errorThrown) {
	console.log("ajaxFailCallback");
	if(xhr.status == 404) {
    	displayAlert("danger", "Server unreachable");
    } else {
    	displayAlert("danger", "Error: "+xhr.status+": "+xhr.statusText);
    }
};